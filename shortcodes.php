<?php
	
function partner_name( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['name'];
}

add_shortcode( 'partner_name', 'partner_name' );


function partner_email( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['email'];
}

add_shortcode( 'partner_email', 'partner_email' );


function partner_phone( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['phone'];
}

add_shortcode( 'partner_phone', 'partner_phone' );


function partner_address( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['address'];
}

add_shortcode( 'partner_address', 'partner_address' );


function partner_zipcode( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['zipcode'];
}

add_shortcode( 'partner_zipcode', 'partner_zipcode' );


function partner_creci( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['creci'];
}

add_shortcode( 'partner_creci', 'partner_creci' );


function partner_real_estate( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['real_estate'];
}

add_shortcode( 'partner_real_estate', 'partner_real_estate' );


function partner_logo( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['logo'];
}

add_shortcode( 'partner_logo', 'partner_logo' );


function partner_picture( $atts ){
	$partner = $_COOKIE['saved_partner'];
	$partner = unserialize(base64_decode($partner));	
	return $partner['picture'];
}

add_shortcode( 'partner_picture', 'partner_picture' );