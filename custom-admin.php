<?php
	
// Partners Custom Fields
	
if(function_exists("register_field_group")){
	register_field_group(array (
		'id' => 'acf_partner-info',
		'title' => 'Partner Info',
		'fields' => array (
			array (
				'key' => 'field_58d9fd38c2680',
				'label' => 'Nome',
				'name' => 'name',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d9fd4bc2681',
				'label' => 'Email',
				'name' => 'email',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d9fd57c2682',
				'label' => 'Telefone',
				'name' => 'phone',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d9fd62c2683',
				'label' => 'Endereço',
				'name' => 'address',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_58d9fd62c2785',
				'label' => 'Zipcode',
				'name' => 'zipcode',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_58d9fd77c2684',
				'label' => 'CRECI',
				'name' => 'creci',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d9fd85c2685',
				'label' => 'Imobiliária',
				'name' => 'real_estate',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d9fdb4c2686',
				'label' => 'Logo',
				'name' => 'logo',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_58d9fdc1c2687',
				'label' => 'Foto',
				'name' => 'picture',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'partners',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

// Partners Custom Post Type
function cptui_register_my_cpts_partners() {

	/**
	 * Post Type: Partners.
	 */

	$labels = array(
		"name" => __( 'Partners', 'sparkling' ),
		"singular_name" => __( 'Partner', 'sparkling' ),
	);

	$args = array(
		"label" => __( 'Partners', 'sparkling' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "partners", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-id",
		"supports" => array( "title", "editor" ),
	);

	register_post_type( "partners", $args );
}

add_action( 'init', 'cptui_register_my_cpts_partners' );


function custom_admin_js() {
    $url = get_bloginfo('template_directory') . '/js/wp-admin.js';
?>
<script>
    jQuery(document).ready(function($){
        // $('#toplevel_page_edit-post_type-acf').remove();
    });
</script>
<?php
}
add_action('admin_footer', 'custom_admin_js');


/* OPTIONS */
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_options',
		'title' => 'Options',
		'fields' => array (
			array (
				'key' => 'field_58e1655e96548',
				'label' => 'Partner URL Parameter',
				'name' => 'url_parameter',
				'type' => 'text',
				'default_value' => 'ap',
				'placeholder' => 'ap',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58e165e613efd',
				'label' => 'Partner Email Selector',
				'name' => 'partner_email_selector',
				'type' => 'text',
				'instructions' => 'CSS Selector for Partner\'s email form field',
				'default_value' => '.partner_email',
				'placeholder' => '.partner_email',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
