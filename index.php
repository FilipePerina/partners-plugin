<?php
   /*
   Plugin Name: InvestCasas Partners
   Plugin URI: http://investcasas.com
   Description: InvestCasas Partners
   Version: 1.0
   Author: Filipe Perina
   Author URI: http://filipeperina.com.br
   License: GPL2
   */



   add_filter('advanced-custom-fields/settings/show_admin', 'my_acf_show_admin');

   function my_acf_show_admin( $show ) {
       return false;
   }

   include_once( 'advanced-custom-fields/acf.php' );
   include_once( 'acf-options-page/acf-options-page.php' );
   include_once( 'acf-repeater/acf-repeater.php' );
   
   include( 'custom-admin.php' );
   include( 'shortcodes.php' );

   $URLParameter = get_field('url_parameter', 'options');   

   $ip = $_SERVER['REMOTE_ADDR'];
   $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
   $zipcode = "{$details->postal}";
   
   $posts = get_posts(array(
		'numberposts'	=> 1,
		'post_type'		=> 'partners',
		'meta_query'	=> array(
		'relation'		=> 'OR',
			array(
				'key'		=> 'zipcode',
				'compare'	=> 'LIKE',
				'value'		=> '',
			),
		)
	));
			          
    // Front End
    
	function updateUrl() {
		global $URLParameter;
		
		if($_COOKIE['saved_partner']){ 
		    $saved_partner =  $_COOKIE['saved_partner'];
			$saved_partner = unserialize(base64_decode($saved_partner));
		    $logo = $saved_partner['logo'];
		    $slug = $saved_partner['slug'];
		    
		    $base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
			$current_url = $base_url . $_SERVER["REQUEST_URI"];
			 
			 $partner_url = add_query_arg( array(
			    ${'URLParameter'} => $slug
			), $current_url );
			
			// echo $current_url . "<br/>" . $partner_url;
			 
			if($current_url !== $partner_url){
				wp_redirect( $partner_url );
			}
	 	}
	}
	 
	add_action('init', 'investCasasInit');	 
	
	function investCasasInit() {
		global $URLParameter;
		global $zipcode;
		
		
		
		updateUrl();	 	
	 	
	 	if(isset($_GET[$URLParameter]) && $_GET[$URLParameter]){
	    
		    $slug = $_GET[$URLParameter];
		    		    
		    
		    $partner = get_page_by_path( $slug, OBJECT, 'partners');
		    		    
		    if($partner) {
				getPartner($partner);			
		    }	    
    	}else{
	    	$partners = get_posts(array(
				'numberposts'	=> 1,
				'post_type'		=> 'partners',
				'meta_query'	=> array(
				'relation'		=> 'OR',
					array(
						'key'		=> 'zipcode',
						'compare'	=> 'LIKE',
						'value'		=> $zipcode,
					),
				)
			));
			
			if($partners && !$_COOKIE['saved_partner']){
				getPartner($partners[0]);	
			}
    	}
	}
	
	function getPartner($partner){
		$partner_id = $partner->ID;
	    $name 		= get_field('name', $partner_id);
	    $email 		= get_field('email', $partner_id);
	    $phone 		= get_field('phone', $partner_id);
	    $address 	= get_field('address', $partner_id);
	    $zip	 	= get_field('zipcode', $partner_id);
	    $creci 		= get_field('creci', $partner_id);
	    $real_estate = get_field('real_estate', $partner_id);
	    $logo 		= get_field('logo', $partner_id);
	    $picture 	= get_field('picture', $partner_id);
	    
	    $partner_data = array(
		    'ID' => $partner_id,
		    'slug' => $partner->post_name,
		    'name' => $name,
		    'email' => $email,
		    'phone' => $phone,
		    'address' => $address,
		    'zipcode' => $zip,
		    'creci' => $creci,
		    'real_estate' => $real_estate,
		    'logo' => $logo,
		    'picture' => $picture
	    );
	    
	    setcookie('saved_partner', base64_encode(serialize($partner_data)), time() + (10 * 365 * 24 * 60 * 60), '/');
		
		$base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
		$current_url = $base_url . $_SERVER["REQUEST_URI"];
		 
		 $partner_url = add_query_arg( array(
		    ${'URLParameter'} => $slug
		), $current_url );
		
		 
		// refresh page if partner date is new
		if(base64_encode(serialize($partner_data)) !== $_COOKIE['saved_partner']){
			wp_redirect( $partner_url );
		}
	}
    
    
    function footerScript() {
	    if( wp_script_is( 'jquery', 'done' ) ) {
		    
		    if($_COOKIE['saved_partner']){ 
			    $saved_partner =  $_COOKIE['saved_partner'];
				$saved_partner = unserialize(base64_decode($saved_partner));
			    $logo = $saved_partner['logo'];
			    $slug = $saved_partner['slug'];
			    
			    $email = $saved_partner['email'];
			    
			    $email_selector = get_field('partner_email_selector', 'options');
			    			    
	    ?>
		    <script type="text/javascript">
		    	jQuery(document).ready(function($){
			    	<?php if($logo){ ?>
			    	$('#logo img').attr('src', '<?= $logo ?>');
			    	<?php } ?>
			    	<?php if($email){ ?>
			    	$('<?= $email_selector ?>').val('<?= $email ?>');
			    	<?php } ?>
			    	
		    	});
		    </script>
	    <?php
		    }
	    }
	}
	add_action( 'wp_footer', 'footerScript' );

?>
